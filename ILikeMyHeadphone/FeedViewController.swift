//
//  FeedViewController.swift
//  ILikeMyHeadphone
//
//  Created by aluno-r17 on 09/07/15.
//  Copyright (c) 2015 Deway. All rights reserved.
//

import UIKit

class FeedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet var tableView: UITableView!
    var reviews : [Review] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        

        var login = true
        
        if !login{
                self.performSegueWithIdentifier("Login", sender: self)
        }
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 61.0/255.0, green: 88.0/255.0, blue: 148.0/255.0, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        
        self.title = "I Like My Headphone"
        
        getReviews()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.reviews.count
    }
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        var cell: FeedTableViewCell = tableView.dequeueReusableCellWithIdentifier("feedCell", forIndexPath: indexPath) as! FeedTableViewCell
        
        var review: Review = self.reviews[indexPath.row]
        cell.brandLabel.text = review.brand
        cell.modelLabel.text = review.model
        cell.commentLabel.text = review.comment
        
        var ratingName = "rating-\(review.rating!)-stars"
        cell.ratingImageView.image = UIImage(named: ratingName)
        
        return cell
        
        
        
        
    }
    
    
    // MARK: -Dados
    func getReviews() {
        var review = Review()
        
        review.brand = "Sony"
        review.model = "AX289"
        review.rating = 4
        review.comment = "Otimo fone de ouvido, gostei bastante"
        review.photo = "http://"
        self.reviews.append(review)
        
        var review2 = Review()
        review2.brand = "Beats"
        review2.model = "SOLA 3D"
        review2.rating = 5
        review2.comment = "Otimo fone de ouvido, gostei bastante"
        review2.photo = "http://"
        self.reviews.append(review2)
        
        var review3 = Review()
        review3.brand = "Panasonic"
        review3.model = "AGHGAK"
        review3.rating = 5
        review3.comment = "Otimo fone de ouvido, gostei bastante"
        review3.photo = "http://"
        self.reviews.append(review3)
        
    }
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
