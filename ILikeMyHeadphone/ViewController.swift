//
//  ViewController.swift
//  ILikeMyHeadphone
//
//  Created by aluno-r17 on 09/07/15.
//  Copyright (c) 2015 Deway. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet var textL1: UITextField!
    
    @IBOutlet var textL2: UITextField!
    
    
    
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
        self.title = "Login"
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
    }
    
    @IBAction func btL1(sender: AnyObject) {
        var alerta = UIAlertView()
        
        if !isValidEmail(self.textL1.text){
            alerta.title = "Error"
            alerta.message = "E-mail invalido"
            alerta.addButtonWithTitle("OK")
            alerta.show()
            return
        }
        
        if count(self.textL2.text) < 6 {
            alerta.title = "Error"
            alerta.message = " A senha deve conter o minimo de 6 caracteres"
            alerta.addButtonWithTitle("OK")
            alerta.show()
            return
        }
        
        alerta.title = "Error"
        alerta.message = "Login ok"
        alerta.addButtonWithTitle("Ok")
        alerta.show()
        
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            //<#code#>
            })
        
    }

    @IBAction func btL2(sender: AnyObject) {
    }
    
    @IBAction func btL3(sender: AnyObject) {
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    
    
//    func validateEmail(candidate : String) -> Bool {
//        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
//        return NSPredicate(format: "SELF METCHES %@", emailRegex).evaluateWithObject(candidate)
//    }
    
    func isValidEmail(testStr:String) -> Bool {
        
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }


}

