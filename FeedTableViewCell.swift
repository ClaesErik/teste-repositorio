//
//  FeedTableViewCell.swift
//  ILikeMyHeadphone
//
//  Created by aluno-r17 on 10/07/15.
//  Copyright (c) 2015 Deway. All rights reserved.
//

import UIKit

class FeedTableViewCell: UITableViewCell {

    @IBOutlet var brandLabel: UILabel!
    @IBOutlet var modelLabel: UILabel!
    @IBOutlet var commentLabel: UILabel!
    @IBOutlet var ratingImageView: UIImageView!
    @IBOutlet var photoImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
